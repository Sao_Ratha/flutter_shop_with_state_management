package com.example.android_pro;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

public class MultipleChiceDialogActivity extends AppCompatActivity {
    String[] colorList;
    boolean[] isChecked;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiple_chice_dialog);

        Button btnMultipleDia = findViewById(R.id.btnMultipleDia);
        TextView textView = findViewById(R.id.txtShow);
        btnMultipleDia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // initialize list
                colorList = new String[]{
                        "Black", "Teal", "Purple",
                };
                isChecked = new boolean[]{
                        false, false, false,
                };

                //
                List<String> colors = Arrays.asList(colorList);

                btnMultipleDia.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MultipleChiceDialogActivity.this);
                        builder.setTitle("Choose your item").setMultiChoiceItems(colorList, isChecked, new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i, boolean b) {
                                isChecked[i] = b;
                                String currentItem = colors.get(i);
                                Toast.makeText(MultipleChiceDialogActivity.this, currentItem, Toast.LENGTH_SHORT).show();
                            }
                        }).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                textView.setText("You have chosen color: ... \n");
                                for(int j=0; j<colors.size(); j++){
                                    boolean c = isChecked[j];
                                    if(c){
                                        textView.setText(textView.getText() + colors.get(j)+ "\n");
                                    }
                                }
                            }
                        });

                        // create dialog
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();

                    }
                });


            }
        });

    }
}