package com.example.android_pro;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

public class SpinnerDemoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner_demo);

        String[] arr = {"a", "b", "c"};

        // Create array adapter
        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(
                this, R.array.country, android.R.layout.simple_expandable_list_item_1
        );

        // set DropDown View Resource
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_expandable_list_item_1);

        // access to component to set data on
        Spinner spinner = findViewById(R.id.txtSpinner);
        spinner.setAdapter(arrayAdapter);

        TextView textView = findViewById(R.id.txtViewShow);

        // for get value when user selected
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // set data to TextView
                textView.setText(String.valueOf(adapterView.getItemAtPosition(i)));
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

    }
}