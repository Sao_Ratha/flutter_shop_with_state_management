package com.example.android_pro;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

public class PopupManuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popup_manu);

        TextView textView = findViewById(R.id.testPopupMenuId);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(PopupManuActivity.this, view);
                popupMenu.setGravity(Gravity.LEFT);

                // inflater Menu resource to Popup Menu
                MenuInflater menuInflater = popupMenu.getMenuInflater();
                menuInflater.inflate(R.menu.my_menu, popupMenu.getMenu());

                // handle when user selected
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()){
                            case R.id.settingId:
                                Toast.makeText(PopupManuActivity.this, "Setting selected", Toast.LENGTH_SHORT).show();
                            case R.id.shareId:
                                Toast.makeText(PopupManuActivity.this, "Share selected", Toast.LENGTH_SHORT).show();
                            case R.id.exitId:
                                Toast.makeText(PopupManuActivity.this, "Exit selected", Toast.LENGTH_SHORT).show();
                        }
                        return true;
                    }
                });

                popupMenu.show();
            }
        });


    }
}