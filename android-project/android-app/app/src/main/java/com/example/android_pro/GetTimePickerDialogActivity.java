package com.example.android_pro;

import androidx.appcompat.app.AppCompatActivity;

import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

public class GetTimePickerDialogActivity extends AppCompatActivity {

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_time_picker_dialog);

        EditText editText = findViewById(R.id.editText);
        TextView textView = findViewById(R.id.textViewPicker);
        Button btnGet = findViewById(R.id.btnGetDate);
        context = GetTimePickerDialogActivity.this;

        editText.setOnClickListener(new View.OnClickListener() {

            TimePickerDialog timePickerDialog;

            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                int minute= calendar.get(Calendar.MINUTE);

                timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener(){

                    @Override
                    public void onTimeSet(TimePicker timePicker, int i, int i1) {
                        editText.setText(i + ":"+ i1);
                    }
                }, hour, minute, false);
                timePickerDialog.show();

            }
        });

        btnGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textView.setText(editText.getText());
            }
        });

    }
}