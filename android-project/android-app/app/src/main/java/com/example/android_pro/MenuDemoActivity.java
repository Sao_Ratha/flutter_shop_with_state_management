package com.example.android_pro;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MenuDemoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_demo);

        TextView textAnim = findViewById(R.id.txtTestingMenuId);
        registerForContextMenu(textAnim);
    }

    // -------------- Using and handle with Option Floating Context Menu, when user sang kot --------------
    // -------------- Using and handle with Option Contextual Menu, show on appBar when user long click --------------

    // for register menuNameResource that create in menu folder with MenuInflater
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_menu, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    // for handle with Option Menu Context

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        // it provide item callback from parameter, we can handle with user select item
        switch (item.getItemId()){
            case  R.id.settingId:{
                Toast.makeText(this, "Menu Setting is elected", Toast.LENGTH_SHORT).show();break;
            }
            case R.id.shareId:{Toast.makeText(this, "Menu Share is selected", Toast.LENGTH_SHORT).show();break;
            }
            case  R.id.exitId:{
                Toast.makeText(this, "Menu Exit is selected", Toast.LENGTH_SHORT).show();break;
            }
        }
        return super.onContextItemSelected(item);
    }

    // -------------- Using and handle with Option Menu --------------

    // for register menu with MenuInflater
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // for handle with Option Menu
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // it provide item callback from parameter, we can handle with user select item
        switch (item.getItemId()){
            case  R.id.settingId:{
                Toast.makeText(this, "Menu Setting is elected", Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.shareId:{
                Toast.makeText(this, "Menu Share is selected", Toast.LENGTH_SHORT).show();
                break;
            }
            case  R.id.exitId:{
                Toast.makeText(this, "Menu Exit is selected", Toast.LENGTH_SHORT).show();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

}