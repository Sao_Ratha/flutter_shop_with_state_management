package com.example.android_pro;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.card.MaterialCardView;
import com.google.android.material.snackbar.Snackbar;

public class SnackbarCardViewActivity extends AppCompatActivity {

    private ConstraintLayout parent;
    private Button btnShowSnackBar;
    private MaterialCardView cardView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snackbar_card_view);

        parent = findViewById(R.id.parent);
        btnShowSnackBar = findViewById(R.id.btnSnackbar);
        cardView = (MaterialCardView) findViewById(R.id.cardViewT);

        btnShowSnackBar.setOnClickListener(v -> {
            showSnackBar();
        });

        cardView.setOnClickListener(v -> {
            Toast.makeText(SnackbarCardViewActivity.this, "MaterialCardView Clicked", Toast.LENGTH_SHORT).show();

        });

    }

    void showSnackBar(){
        Snackbar.make(parent, "This is snackbar", Snackbar.LENGTH_INDEFINITE)
                .setAction("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(SnackbarCardViewActivity.this, "Retry", Toast.LENGTH_SHORT).show();
                    }
                })
                .setTextColor(getResources().getColor(R.color.purple_500))
                .setActionTextColor(getResources().getColor(R.color.purple_500))
                .setBackgroundTint(getResources().getColor(R.color.teal_200))
                .show();
    }

}