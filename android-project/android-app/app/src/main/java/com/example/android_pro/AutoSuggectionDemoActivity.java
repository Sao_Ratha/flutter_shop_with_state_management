package com.example.android_pro;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import java.util.List;

public class AutoSuggectionDemoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auto_suggection_demo);


        // make list of suggestion
        String[] valueSuggestion = {"Ratha", "Pich", "Nery Long", "Kimcheng"};

        // Create adapter
        ArrayAdapter<String> myAdapter = new ArrayAdapter<>(
                AutoSuggectionDemoActivity.this,
                android.R.layout.simple_list_item_1,
                valueSuggestion
                );

        // Access to TextView and set Data
        AutoCompleteTextView autoCompleteTextView = findViewById(R.id.txtAutoCompleteText);
        autoCompleteTextView.setAdapter(myAdapter);

    }
}