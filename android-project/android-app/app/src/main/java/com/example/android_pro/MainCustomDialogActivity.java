package com.example.android_pro;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainCustomDialogActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_custom_dialog);

        Button  btnShowCustomDia = findViewById(R.id.btnShowCustomDia);
        TextView textViewResult = findViewById(R.id.textViewResult);
        btnShowCustomDia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog customDialog = new Dialog(MainCustomDialogActivity.this);
                customDialog.setContentView(R.layout.activity_custom_dialog);
                customDialog.setCancelable(false);

                // dismiss, make sure user can cancel dialog anywhere
                //customDialog.dismiss();

                // Access to View in Dialog
                Button btnOk = customDialog.findViewById(R.id.btnOk);
                TextView txtEditPrice = customDialog.findViewById(R.id.txtEditPrice);
                btnOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        textViewResult.setText(txtEditPrice.getText());
                        customDialog.dismiss();
                    }
                });


                customDialog.show();
            }
        });

    }
}