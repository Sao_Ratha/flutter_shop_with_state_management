package com.example.android_pro;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class TestDifferentXmlActivity extends AppCompatActivity {

    private TextView txtHello;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_different_xml);

        txtHello = findViewById(R.id.txtHello);
        txtHello.setText(getString(R.string.hello));

    }
}