package com.example.android_pro;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // for convert layoutName.xml become to Object in this class
        setContentView(R.layout.activity_main);

        TextView textView = findViewById(R.id.textView);
        ImageView imageView = findViewById(R.id.imageView);

        // data pass from login screen
        // Intent intent = getIntent();
        //String data = intent.getStringExtra("loginKey");
        //.setText(data);


        //textView.setText("Hiiiii");
        // set textview from resource with java code
        //textView.setText(getResources().getString(R.string.my_app));

        // set image from resource with java code
        //imageView.setImageDrawable(getResources().getDrawable(R.drawable.flutter_img));

    }
}
