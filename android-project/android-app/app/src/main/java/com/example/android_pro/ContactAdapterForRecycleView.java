package com.example.android_pro;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ContactAdapterForRecycleView extends RecyclerView.Adapter<ContactAdapterForRecycleView.ContactViewHolder>{

    Context context;
    List<ContactModel> contactModelList;
    OnProfileClickListener onProfileClickListener;

    public ContactAdapterForRecycleView(Context context, List<ContactModel> contactModelList, OnProfileClickListener onProfileClickListener) {
        this.context = context;
        this.contactModelList = contactModelList;
        this.onProfileClickListener = onProfileClickListener;

    }

    // Step3: extends RecyclerView.Adapter of ModelViewHolder in nested class and override three method
    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Inflater CustomUI activity_recycle_view_custom
        View view = LayoutInflater.from(context)
                .inflate(R.layout.activity_recycle_view_custom, parent, false);
        return new ContactViewHolder(view);
    }

    // onBindViewHolder for get data from list and bind set it to holder of Model
    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder holder, int position) {
        ContactModel contactModel = contactModelList.get(position);
        holder.name.setText(contactModel.getContactName());
        holder.phone.setText(contactModel.getContactPhone());
        holder.profile.setImageDrawable(context.getDrawable(contactModel.getImageProfile()));

        holder.parent_recycle_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onProfileClickListener.onClickListener(contactModel);
                //Toast.makeText(context, holder.name.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return contactModelList.size();
    }

    // Step2: Create ModelNameViewHolder for hold data of Model
    class ContactViewHolder extends RecyclerView.ViewHolder {
        ImageView profile;
        TextView name, phone;
        ConstraintLayout parent_recycle_layout;

        public ContactViewHolder(@NonNull View itemView) {
            super(itemView);
            profile = itemView.findViewById(R.id.imageView);
            name = itemView.findViewById(R.id.contact_name);
            phone = itemView.findViewById(R.id.contact_phone);
            parent_recycle_layout = itemView.findViewById(R.id.parent_recycle_layout);
        }
    }

    // for handle when user click
    public interface OnProfileClickListener{
        void  onClickListener(ContactModel contact);
    }

}
