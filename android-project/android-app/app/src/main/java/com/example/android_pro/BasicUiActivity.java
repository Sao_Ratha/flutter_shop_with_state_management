package com.example.android_pro;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Toast;

public class BasicUiActivity extends AppCompatActivity {

    private CheckBox chMale, chFemale, chBoth;
    private RadioGroup parentRadioGroup;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_ui);

        chMale = findViewById(R.id.male);
        chFemale = findViewById(R.id.female);
        chBoth = findViewById(R.id.bothMaleFemale);
        parentRadioGroup = findViewById(R.id.parentRadioGroup);
        progressBar = findViewById(R.id.progressBar);


        // Handle on Checkbox
        chMale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
               if(isChecked){
                   Toast.makeText(BasicUiActivity.this, "U are male", Toast.LENGTH_SHORT).show();

               }else{
                   Toast.makeText(BasicUiActivity.this, "U not are male", Toast.LENGTH_SHORT).show();
               }
            }
        });

        // handle on Radio Group when click
        parentRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.radioMarried:
                        Toast.makeText(BasicUiActivity.this, "Married", Toast.LENGTH_SHORT).show();
                    break;
                    case  R.id.radioSingle:
                        Toast.makeText(BasicUiActivity.this, "Single", Toast.LENGTH_SHORT).show();

                        // Handle change on ProgressBar
                        //progressBar.setVisibility(View.VISIBLE);

                    break;
                    case  R.id.radioRelationship:
                        Toast.makeText(BasicUiActivity.this, "Relationship", Toast.LENGTH_SHORT).show();

                        // Handle change on ProgressBar
                        //progressBar.setVisibility(View.GONE);
                }
            }
        });

        // handle on Radio Group without onClick
        int checked = parentRadioGroup.getCheckedRadioButtonId();
        switch (checked){
            case R.id.radioMarried:
                Toast.makeText(BasicUiActivity.this, "Married", Toast.LENGTH_SHORT).show();
                break;
            case  R.id.radioSingle:
                Toast.makeText(BasicUiActivity.this, "Single", Toast.LENGTH_SHORT).show();
                break;
            case  R.id.radioRelationship:
                Toast.makeText(BasicUiActivity.this, "Relationship", Toast.LENGTH_SHORT).show();
        }

        // handle on ProgressBar
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                for(int i=0; i<10; i++){
                    progressBar.incrementProgressBy(10);
                    SystemClock.sleep(500);
                }
            }
        });
        thread.start();

        System.out.println("progressBar= "+progressBar.getProgress());



    }
}