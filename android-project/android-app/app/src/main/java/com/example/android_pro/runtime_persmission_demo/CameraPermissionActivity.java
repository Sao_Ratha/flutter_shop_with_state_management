package com.example.android_pro.runtime_persmission_demo;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.android_pro.R;

public class CameraPermissionActivity extends AppCompatActivity {

    ImageView image;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_permission);

        image = findViewById(R.id.image);
        context = this;
        image.setOnClickListener(v -> {
            selectImage(context);
        });

    }

    void  selectImage(Context context){
        final  CharSequence[] options = {"Take image", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Choose Image");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(options[which].equals("Take image")){
                    // Show Camera
                    Intent takePhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePhoto, 0);
                    //someActivityResultLauncher.launch(takePhoto);  // new way update on startActivityForResult
                }else if(options[which].equals("Choose from Gallery")){
                    Intent pickImage = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickImage, 1);

                }else if(options[which].equals("Cancel")){
                    Toast.makeText(context, "Cancel", Toast.LENGTH_SHORT).show();
                }

            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RESULT_OK){
            switch (resultCode){
                case 0:
                    if(data != null){
                        Bitmap selectImage = (Bitmap) data.getExtras().get("data");
                        image.setImageBitmap(selectImage);
                    }
                break;
                case 1:
                    if(data != null) {
                        Uri selectImage = data.getData();

                    }
                break;
            }
        }

    }

    /*
    ActivityResultLauncher<Intent> someActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        // There are no request codes
                        //Intent data = result.getData();
                        //doSomeOperations();

                        Intent data = result.getData();
                        //image.setImageBitmap(data.);
                    }
                }
            });
     */


}