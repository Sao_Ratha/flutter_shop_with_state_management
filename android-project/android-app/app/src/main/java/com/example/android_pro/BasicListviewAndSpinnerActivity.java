package com.example.android_pro;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class BasicListviewAndSpinnerActivity extends AppCompatActivity {

    private ListView listCities;
    private Spinner studentSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_listview_and_spinner);

        listCities = findViewById(R.id.listCities);
        studentSpinner = findViewById(R.id.studentSpinner);

        // prepare data resource for student spinner
        ArrayList<String> students = new ArrayList<>();
        students.add("Ratha");
        students.add("Kimlong");
        students.add("Kimchheng");
        students.add("Tean");

        // prepare adapter
        ArrayAdapter<String> studentAdapter = new ArrayAdapter<String>(
                this,
                androidx.appcompat.R.layout.support_simple_spinner_dropdown_item,
                students
        );

        studentSpinner.setAdapter(studentAdapter);

        // handle
        studentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // handle when assign student dynamic on Java file
                Toast.makeText(BasicListviewAndSpinnerActivity.this, students.get(position)+ " Selected", Toast.LENGTH_SHORT).show();
                // handle when assign student dynamic on layout by entry, this idea without make data adapter on java
                //Toast.makeText(BasicListviewAndSpinnerActivity.this, studentSpinner.getSelectedItem().toString()+ " Selected", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(BasicListviewAndSpinnerActivity.this, "None", Toast.LENGTH_SHORT).show();
            }
        });


        // prepare data resource for city list
        ArrayList<String> cities = new ArrayList<String>();
        cities.add("PP City");
        cities.add("KS City");
        cities.add("BTB City");
        cities.add("Kanda City");
        cities.add("Duma City");

        // make adapter
        ArrayAdapter<String> citiesAdapter = new ArrayAdapter<String>(
                this,
                androidx.appcompat.R.layout.support_simple_spinner_dropdown_item,
                cities
        );

        listCities.setAdapter(citiesAdapter);

        // handle on click item of list
        listCities.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(BasicListviewAndSpinnerActivity.this, cities.get(position)+ " Selected", Toast.LENGTH_SHORT).show();
            }
        });





    }
}