package com.example.android_pro;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.security.acl.Group;

public class UsingImageView_RadioBottonActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_using_image_view_radio_botton);

        ImageView imageView = findViewById(R.id.imageViewId);
        String imageUrl = "https://images.fineartamerica.com/images/artworkimages/mediumlarge/1/nice-landscape-claudio-jose.jpg";
        Glide.with(UsingImageView_RadioBottonActivity.this).load(imageUrl).into(imageView);

        // handle on radio group
        RadioGroup radioGroup = findViewById(R.id.radioGroup);
        radioGroup.clearCheck();
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton radioButton = radioGroup.findViewById(i);
                Toast.makeText(UsingImageView_RadioBottonActivity.this, radioButton.getText(), Toast.LENGTH_LONG).show();
            }
        });


    }
}