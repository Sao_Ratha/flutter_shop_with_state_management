package com.example.android_pro.data_storage_demo.share_reference_demo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android_pro.R;

public class ShareReferenceActivity extends AppCompatActivity implements View.OnClickListener {

    EditText name, phone;
    Button btnSave, btnClear, btnRetrive;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_reference);

        initView();
        sharedPreferences = getSharedPreferences("my-preference", MODE_PRIVATE);

        btnSave.setOnClickListener(this);
        btnClear.setOnClickListener(this);
        btnRetrive.setOnClickListener(this);

    }

    void initView(){
         name = findViewById(R.id.name);
         phone = findViewById(R.id.phone);
         btnSave = findViewById(R.id.btnSave);
         btnClear = findViewById(R.id.btnClear);
         btnRetrive = findViewById(R.id.btnRetrieve);
    }

    @Override
    public void onClick(View view) {
      if(view == btnSave){
          String nameData = name.getText().toString();
          String phoneData = phone.getText().toString();

          System.out.println("Name:"+nameData);
          System.out.println("Phone:"+phoneData);

          SharedPreferences.Editor editor = sharedPreferences.edit();
          editor.putString("key_name", nameData);
          editor.putString("key_phone", phoneData);
          //editor.putBoolean("isDartMode", true);

          name.setText("");
          phone.setText("");

          editor.apply();
          Toast.makeText(this,"Data has been saved", Toast.LENGTH_SHORT).show();
      }
      if(view == btnRetrive){
          String getName = sharedPreferences.getString("key_name", "");
          String getPhone = sharedPreferences.getString("key_phone", "");
          name.setText(getName);
          phone.setText(getPhone);

          Toast.makeText(this, "Retrieve Data", Toast.LENGTH_SHORT).show();

      }

      if(view == btnClear){
          name.setText("");
          phone.setText("");

          sharedPreferences.edit().clear().apply();

      }

    }
}