package com.example.android_pro;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class BasicConstraintLayoutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_constraint_layout);
    }
}