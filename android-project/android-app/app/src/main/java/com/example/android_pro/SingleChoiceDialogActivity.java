package com.example.android_pro;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SingleChoiceDialogActivity extends AppCompatActivity {

    String[] dialogItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_choice_dialog);

        Button btnSingleDia = findViewById(R.id.btnSingleChoiceDia);
        TextView textView = findViewById(R.id.txtResultSingleChoiceDia);
        dialogItem = getResources().getStringArray(R.array.dialog_item);

        btnSingleDia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // make Alter build
                AlertDialog.Builder builder = new AlertDialog.Builder(SingleChoiceDialogActivity.this);
                // set value
                builder.setTitle("Please choice one").setSingleChoiceItems(
                        dialogItem, -1, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                textView.setText(dialogItem[i]);
                                dialogInterface.dismiss();
                            }
                        }
                );
                // Create Alert Dialog
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });




    }
}