package com.example.android_pro;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    int REQUEST_CODE = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button btnNext = findViewById(R.id.nextId);

        btnNext.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, LoginDetailActivity.class);
                intent.putExtra("loginKey", "Data from Login screen");

                //startActivity(intent); // for push to other activity without get data back
                startActivityForResult(intent, REQUEST_CODE); // for push to other activity and get data back also

                // Navigator to other website
                //Intent intent = new Intent(Intent.ACTION_VIEW);
                //intent.setData(Uri.parse("https://www.bbc.com/news/uk"));
                //startActivity(intent);

            }
        });
    }

    // for get data back from screen B
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if(requestCode == REQUEST_CODE){
            if(resultCode == RESULT_OK){
                String dataFromDetail = intent.getStringExtra("loginKeyDetail");
                Toast.makeText(LoginActivity.this, dataFromDetail, Toast.LENGTH_LONG).show();
            }
        }
    }
}