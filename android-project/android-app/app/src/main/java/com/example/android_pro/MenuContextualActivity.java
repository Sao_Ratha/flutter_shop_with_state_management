package com.example.android_pro;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MenuContextualActivity extends AppCompatActivity implements ActionMode.Callback{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_contextual);

        Button btnContextualMenu = findViewById(R.id.btnContextualMeno);
        btnContextualMenu.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View view) {
                startActionMode(MenuContextualActivity.this, ActionMode.TYPE_PRIMARY);
                return true;
            }
        });

    }

    // The 4 override method is override from implement ActionMode.Callback

    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        // for inflater my_menu
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        // it provide item callback from parameter, we can handle with user select item
        switch (menuItem.getItemId()){
            case  R.id.settingId:{
                Toast.makeText(this, "Menu Setting is elected", Toast.LENGTH_SHORT).show();break;
            }
            case R.id.shareId:{Toast.makeText(this, "Menu Share is selected", Toast.LENGTH_SHORT).show();break;
            }
            case  R.id.exitId:{
                Toast.makeText(this, "Menu Exit is selected", Toast.LENGTH_SHORT).show();break;
            }
        }
        return true;
    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode) {

    }
}