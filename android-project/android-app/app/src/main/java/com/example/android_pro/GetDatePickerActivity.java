package com.example.android_pro;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

public class GetDatePickerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_date_picker);

        DatePicker datePicker = findViewById(R.id.datePicker);
        TextView textView = findViewById(R.id.txtGetDate);
        Button btnDate = findViewById(R.id.btnDate);

        btnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textView.setText(String.valueOf(datePicker.getDayOfMonth() + "/"+datePicker.getMonth()+ "/"+datePicker.getYear()));
            }
        });

    }
}