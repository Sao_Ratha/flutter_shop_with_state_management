package com.example.android_pro;

// Step1:  Create Model for hold data of UI
public class ContactModel {

    String contactName;
    String contactPhone;
    int imageProfile;

    public ContactModel(String contactName, String contactPhone, int profile) {
        this.contactName = contactName;
        this.contactPhone = contactPhone;
        this.imageProfile = profile;
    }

    public String getContactName() {
        return contactName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public int getImageProfile() {
        return imageProfile;
    }

    public void setContactName(String contactName){
        this.contactName = contactName;
    }

    public void  setContactPhone(String contactPhone){
        this.contactPhone = contactName;
    }

    public void setContactImageProfile(int profile){
        this.imageProfile = profile;
    }

}
