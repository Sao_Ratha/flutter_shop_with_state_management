package com.example.android_pro;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class LoginDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_detail);

        TextView view = findViewById(R.id.textLoginDetail);
        Button nextBtn = findViewById(R.id.backButtonId);

        // get data from login
        Intent intent = getIntent();
        String data = intent.getStringExtra("loginKey");
        // set data on login detail
        view.setText(data.toString());

        // set action on BackButton
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.putExtra("loginKeyDetail", "Value from detail screen ");
                setResult(RESULT_OK, intent);
                finish();
            }
        });



    }
}