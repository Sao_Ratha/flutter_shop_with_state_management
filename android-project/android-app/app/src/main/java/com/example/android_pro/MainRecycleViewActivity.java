package com.example.android_pro;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainRecycleViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_recycle_view);

        RecyclerView recyclerView = findViewById(R.id.recycleViewId);

        // initialize data into List of ModelAdapter
        List<ContactModel> contactModelList = new ArrayList<>();
        contactModelList.add(new ContactModel("Ratha", "015883432", R.drawable.ring));
        contactModelList.add(new ContactModel("Dara", "099883432", R.drawable.ring));
        contactModelList.add(new ContactModel("Vesna", "022883432", R.drawable.ring));

        // initialize Adapter
        ContactAdapterForRecycleView adapter = new ContactAdapterForRecycleView(MainRecycleViewActivity.this, contactModelList, new ContactAdapterForRecycleView.OnProfileClickListener() {
            @Override
            public void onClickListener(ContactModel contact) {
                Toast.makeText(MainRecycleViewActivity.this, contact.getContactPhone().toString(), Toast.LENGTH_SHORT).show();
            }
        });

        // initialize RecycleView
        recyclerView.setLayoutManager(new LinearLayoutManager(MainRecycleViewActivity.this));
        // set adapter
        recyclerView.setAdapter(adapter);

    }
}